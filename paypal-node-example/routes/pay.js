const express = require('express');
const paypal = require('paypal-rest-sdk');
const dbCon = require('./../config/dbConfig')
const routes = express.Router();
var details = require('./../app');


var create_payment_json;
let price = 0
let quantity = 0
let total = 0

paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AWSnZhhiBIW8r9jcQVKZkN4eMPhaWYVdnmiXiG1MUJsgxCKooofmrcNmfIB2-RJBBFnyrEpye4REw7jL',
    'client_secret': 'EP4qIRoc5R1Y4BMZh-v-mqbWvDCiD1Noq6PDfafWsxya-9r2x2zthkQYgbIEISa2gAGADwJzyVZMj229'
});


routes.post('/', (req, res) => {
    details.getAmount(req, (data) => {
        price = data.amount
        quantity = 1;
        total = quantity * price;

        let create_payment_json = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://localhost:3000/pay/success",
                "cancel_url": "http://localhost:3000/pay/cancel"
            },
            "transactions": [{
                "item_list": {
                    "items": [{
                        "name": data.name,
                        "sku": "item",
                        "price": price,
                        "currency": "USD",
                        "quantity": quantity
                    }]
                },
                "amount": {
                    "currency": "USD",
                    "total": total
                },
                "description": "Testing Paypal payment integration"
            }]
        };

        console.log(create_payment_json)

        paypal.payment.create(create_payment_json, function (error, payment) {
            if (error) {
                throw error;
            } else {
                payment.links.forEach(link => {
                    if (link.rel == 'approval_url') {
                        res.redirect(link.href)
                    }
                });
            }
        });
    })
})


routes.get('/success', (req, res) => {
    console.log("/success")
    const payerID = req.query.PayerID;
    const paymentID = req.query.paymentId;

    const execute_payment_json = {
        "payer_id": payerID,
        "transactions": [{
            "amount": {
                "currency": "USD",
                "total": total
            }
        }]
    };

    paypal.payment.execute(paymentID, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log(JSON.stringify(payment));
            res.render('success')
        }
    });

})

routes.get('/cancel', (req, res) => {
    res.render('cancel')
})


module.exports = routes;