const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs')
const dbCon = require('./config/dbConfig')

const connection = require("./config/dbConfig");

let dbObj;
const db = dbCon.dbLink((_db) => {
    dbObj = _db.db("payment")
})

var pay = require('./routes/pay');

var app = express();

app.use(bodyParser.json())
app.use(express.urlencoded({ extended: false }))

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index')
});

app.use('/pay', pay);


if (connection.checkLink) {
    app.listen(3000, () => console.log('Server started at port 3000'))
} else {
    console.log("problem in starting server")
}


app.post('/getAmount', (req, res) => {
    this.getAmount(req, (data) => res.send(data))
})


exports.getAmount = (req, callback) => {
    console.log(req.body)
    dbObj.collection("paymentCollection").findOne({ username: req.body.username }, (err, result) => {
        if (err) {
            console.log(err)
        } else
            callback({ message: result.amount, amount: result.amount });
    });
}



